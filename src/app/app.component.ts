import {Component} from '@angular/core';
import {DataInterface} from "./interfaces/dataInterfaces";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  data: DataInterface[][] = [
    [
      {
        name: "Greece",
        auctions: 123455,
        requests: 64728,
        bids: 64728,
        views: 64728,
        clicks: 64728,
        CTR: 64728,
        CPC: 64728
      },
      {
        name: "Georg",
        auctions: 23245,
        requests: 32033,
        bids: 32033,
        views: 32033,
        clicks: 32033,
        CTR: 32033,
        CPC: 32033
      },
      {
        name: "Vietnam",
        auctions: 12345,
        requests: 16398,
        bids: 16398,
        views: 16398,
        clicks: 16398,
        CTR: 16398,
        CPC: 16398
      },
      {
        name: "Falkla",
        auctions: 12345,
        requests: 16398,
        bids: 16398,
        views: 16398,
        clicks: 16398,
        CTR: 16398,
        CPC: 16398
      },
      {
        name: "Denm",
        auctions: 132435,
        requests: 64898,
        bids: 64898,
        views: 64898,
        clicks: 64898,
        CTR: 64898,
        CPC: 64898
      },
      {
        name: "Trinid",
        auctions: 43223,
        requests: 36168,
        bids: 36168,
        views: 36168,
        clicks: 36168,
        CTR: 36168,
        CPC: 36168
      },
      {
        name: "Unite",
        auctions: 43234,
        requests: 88927,
        bids: 88927,
        views: 88927,
        clicks: 88927,
        CTR: 88927,
        CPC: 88927
      }
    ]
  ];

  addTable(): void {
    this.data.push(this.data[0]);
  }
}
