import {Component, EventEmitter, Input, OnInit} from '@angular/core';

import {DataInterface} from "../../interfaces/dataInterfaces";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() items: DataInterface[];
  canShow: boolean = true;
  addEminter: EventEmitter<null> = new EventEmitter();


  emitter: EventEmitter<null> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  emitResize(): void {
    this.emitter.emit();
  }

  getSum(key: string): number {
    return this.items.reduce((val, curr) => val + curr[key], 0);
  }

  hideTable(): void {
    this.canShow = false;
  }

  addRow(): void {
    this.items.push(this.items[5]);
    this.addEminter.emit();
  }
}
