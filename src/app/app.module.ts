import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TableComponent } from './components/table/table.component';
import { ResizeDirective } from './directives/resize.directive';
import { ScrollbarDirective } from './directives/scrollbar.directive';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    ResizeDirective,
    ScrollbarDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
