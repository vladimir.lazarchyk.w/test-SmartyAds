interface DataInterface {
  name: string;
  auctions: number;
  requests: number;
  bids: number;
  views: number;
  clicks: number;
  CTR: number;
  CPC: number;
}

export {DataInterface};
