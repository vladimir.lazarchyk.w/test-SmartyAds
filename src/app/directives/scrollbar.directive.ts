import {Directive, ElementRef, Input, OnInit} from '@angular/core';
import PerfectScrollbar from "perfect-scrollbar";

@Directive({
  selector: '[appScrollbar]'
})
export class ScrollbarDirective implements OnInit{
  ps;
  @Input () resize;
  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    console.log('init');
    this.ps = new PerfectScrollbar(this.element.nativeElement);
    console.log(this.ps);
    this.resize.subscribe(() => {
      this.ps.update();
    })
  }

}
