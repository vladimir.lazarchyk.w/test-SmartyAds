import {AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
declare var $;

@Directive({
  selector: '[appResize]'
})
export class ResizeDirective implements AfterViewInit {
  @Output ('resize') emitter = new EventEmitter();
  @Input() addEmiter;

  constructor(private table: ElementRef) {
  }

  ngAfterViewInit(): void {
    let maxHeight = this.table.nativeElement.children[0].children[0].getBoundingClientRect();
    console.log(maxHeight);
    $(this.table.nativeElement).draggable().resizable({
      maxHeight:  maxHeight.height
    });
    $(this.table.nativeElement).on('resize', () => {
      this.emitter.emit();
    });
    this.addEmiter.subscribe(() => {
      $(this.table.nativeElement).draggable("disable");
      let timeOut = setTimeout(() => {
        maxHeight = this.table.nativeElement.children[0].children[0].getBoundingClientRect();
        console.log(maxHeight.height);
        $(this.table.nativeElement).draggable().resizable({
          maxHeight:  maxHeight.height
        });
        clearTimeout(timeOut)
      }, 1000);
    })
  }

}
